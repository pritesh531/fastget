# Fastget

This project contains service providers application where users get the services they want and setup payment to the service provider.

# Development Info

- The project is based on the Docker Development.
- We have implemented Elastic Search to search the service providers in the database.
- Also, it contains setup to deploy project on Heroku.

## Setup the Project

### Clone the project

Run the following command to clone the project.

```sh
    git clone git@bitbucket.org:pritesh531/fastget.git
    cd fastget/
```

### Docker Setup Commands

The following command which completely setups the project and host on the 127.0.0.1:8000

```sh
    docker-compose up -d --build
```
