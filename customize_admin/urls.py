from django.contrib.auth.decorators import login_required
from django.urls import path

from .views import (CustomerDeleteView, CustomerList,
                    ServiceProviderDeleteView, ServiceProviderList,
                    ServiceProviderProfileView, ServiceProviderRequests,
                    service_provider_request_approve,
                    service_provider_request_deny, home)

urlpatterns = [
    path('service_providers/', login_required(ServiceProviderList.as_view()),
         name="service_providers"),
    path('customers/', login_required(CustomerList.as_view()), name="customers"),
    path('customers/<int:pk>/delete/',
         login_required(CustomerDeleteView.as_view()), name="customer_delete"),
    path('service_provider_requests/', login_required(ServiceProviderRequests.as_view()),
         name="service_provider_requests"),
    path('service_provider_requests/<int:user_id>/approve/',
         login_required(service_provider_request_approve), name="service_provider_request_approve"),
    path('service_provider_requests/<int:user_id>/deny/',
         login_required(service_provider_request_deny), name="service_provider_request_deny"),
    path('service_providers/<int:pk>/delete/',
         login_required(ServiceProviderDeleteView.as_view()), name="service_provider_delete"),
    path('service_providers/<int:pk>/profile/',
         login_required(ServiceProviderProfileView.as_view()), name="service_provider_profile"),
    path('', home, name="home"),
]
