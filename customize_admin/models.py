from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import ugettext_lazy as _
from phonenumber_field.modelfields import PhoneNumberField
from .signals.confirm_registration import confirm_registration


class User(AbstractUser):
    """Extended Django's builtin user model to include profile pic, phone number etc. fields in the user model.
    Make email reuired for user.

    Args:
        AbstractUser ([type]): Django's built in model for saving the user details.
    """
    email = models.EmailField(_('email address'), unique=True)
    profile_pic = models.ImageField(
        _("Profile Picture"), upload_to="profile_pics/")
    phone_number = PhoneNumberField()
    is_service_provider = models.BooleanField(default=False)
    is_customer = models.BooleanField(default=True)


class ServiceProvider(models.Model):
    """Model to store the extra data specific to the service provider.
    """
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    degree = models.CharField(blank=True, max_length=100)
    specialize = models.CharField(blank=True, max_length=100)
    location = models.CharField(max_length=100, blank=True)


# Sends welcome mail to the user or service provider for successful registration.
models.signals.post_save.connect(confirm_registration, sender=User)
