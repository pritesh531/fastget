from django.contrib import messages
from django.contrib.auth import views as auth_views
from django.core.exceptions import PermissionDenied
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse, reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views import generic
from elasticsearch_dsl import Q as es_q

from .documents import ServiceProviderDocument
from .models import ServiceProvider, User
from .signals.send_reset_password_email import reset_password


class UserProfileView(generic.edit.UpdateView):
    """Class based view to update user details in the admin.
    """
    model = User
    fields = ['username', 'email', 'phone_number',
              'profile_pic', 'first_name', 'last_name']
    success_url = reverse_lazy('dashboard')

    def get_object(self):
        return self.model.objects.get(pk=self.request.user.pk)

    def get_success_url(self):
        messages.success(self.request, _("Personal settings updated."))
        return super().get_success_url()


class ServiceProviderProfileView(generic.edit.UpdateView):
    """Class based view to update service provider in the admin.
    """
    model = ServiceProvider
    fields = ['degree', 'specialize', 'location']
    success_url = reverse_lazy('dashboard')
    template_name = 'customize_admin/user_form.html'

    def get_object(self):
        return self.model.objects.get(user=self.request.user)

    def get_success_url(self):
        messages.success(self.request, _("Profile details updated."))
        return super().get_success_url()


class PasswordResetView(auth_views.PasswordResetView):
    """Extended password reset view to change the reset password mail plain to HTML.
    """
    html_email_template_name = 'registration/password_reset_email_html.html'


class ServiceProvideSignUp(generic.CreateView):
    """ServiceProviderSignUp view.
    """
    model = User
    fields = ['username', 'email', 'phone_number']
    template_name = 'registration/service_provider_signup.html'
    success_url = reverse_lazy('login')

    def form_valid(self, form):
        form.instance.is_service_provider = True
        form.instance.is_customer = False
        form.instance.is_active = False
        return super().form_valid(form)

    def get_success_url(self):
        messages.success(
            self.request, _("Service provider %s created.") % self.object.username)
        return super().get_success_url()


class ServiceProviderRequests(generic.ListView):
    """Class based view for displaying service provider requests.
    """
    model = User

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            raise PermissionDenied
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        """Filter queryset to display the service providers requests.
        """
        qs = super().get_queryset()
        if qs:
            qs = qs.filter(Q(is_superuser=False) & Q(is_active=False) & Q(is_service_provider=True))
        return qs

    def get_context_data(self, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        context['title'] = _('Service Provider Requests')
        context['requests'] = True
        return context


class BaseListView(generic.ListView):
    """Base lisr view to provide delete url name of the specific type of users
    and add `delete_url_name` in the template context.
    """
    title = None
    delete_url_name = None

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            raise PermissionDenied
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['delete_url_name'] = self.delete_url_name
        context['title'] = self.title
        return context


class ServiceProviderList(BaseListView):
    """Class based view for service provider list.
    """
    model = User
    delete_url_name = 'service_provider_delete'
    title = _('Service Providers')

    def get_queryset(self):
        """Filter queryset to display the service providers requests.
        """
        qs = super().get_queryset()
        qs = qs.filter(Q(is_superuser=False) & Q(
            is_active=True) & Q(is_service_provider=True))
        return qs


class CustomerList(BaseListView):
    """Class based view for customer list.
    """
    model = User
    delete_url_name = 'customer_delete'
    title = _('Customers')

    def get_queryset(self):
        """Filter queryset to display the service providers requests.
        """
        qs = super().get_queryset()
        qs = qs.filter(Q(is_superuser=False) & Q(is_customer=True))
        return qs


class BaseDeleteView(generic.DeleteView):
    """Base delete view for common methods and attributes like list url name.
    """
    list_url_name = None
    title = None

    def get_context_data(self, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        context['title'] = self.title
        context['list_url_name'] = self.list_url_name
        return context

    def get_success_url(self):
        messages.success(self.request, _('%s %s deleted.' %
                                         (self.model._meta.verbose_name, self.object.username)))
        return super().get_success_url()


class ServiceProviderDeleteView(BaseDeleteView):
    """Class based view for service provider delete view.
    """
    model = User
    list_url_name = "service_providers"
    success_url = reverse_lazy('service_providers')
    title = _('Service Providers')

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            raise PermissionDenied

        self.user = get_object_or_404(User, pk=kwargs['pk'])

        if not self.user.is_service_provider:
            raise PermissionDenied

        return super().dispatch(request, *args, **kwargs)


class CustomerDeleteView(BaseDeleteView):
    """Class based view for customer delete view.
    """
    model = User
    title = _('Customers')
    list_url_name = "customers"
    success_url = reverse_lazy('customers')

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            raise PermissionDenied

        self.user = get_object_or_404(User, pk=kwargs['pk'])

        if not self.user.is_customer:
            raise PermissionDenied

        return super().dispatch(request, *args, **kwargs)


def service_provider_request_approve(request, user_id):
    """Admin user approves the user as service provider.
    """
    if not request.user.is_superuser:
        raise PermissionDenied

    user = get_object_or_404(User, pk=user_id)
    user.is_active = True
    user.save()

    ServiceProvider.objects.create(user=user)

    reset_password(request, user.email)

    messages.success(
        request, "%s approved as service provider." % user.username)

    return HttpResponseRedirect(reverse('service_provider_requests'))


def service_provider_request_deny(request, user_id):
    """Admin user denies the user as service provider.
    """
    if not request.user.is_superuser:
        raise PermissionDenied

    user = get_object_or_404(User, pk=user_id)

    if user.is_superuser or user.is_customer or not user.is_service_provider:
        raise PermissionDenied

    user.delete()

    messages.success(
        request, "%s not approved as service provider." % user.username)

    return HttpResponseRedirect(reverse('service_provider_requests'))


def home(request):
    query = request.GET.get('q', None)

    service_providers = []

    if query:
        query = es_q("multi_match", query=query)
        search = ServiceProviderDocument.search()
        service_providers = search.query(query)
    return render(request, 'home.html', {
        'service_providers': service_providers,
    })
