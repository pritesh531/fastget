from django.conf import settings
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.utils.translation import gettext_lazy as _


def confirm_registration(sender, instance, **kwargs):
    """Post save signal to send the mail to the user or service provider for welcoming to the site.
    """
    if not instance.is_superuser:
        if kwargs['created']:
            html_message = render_to_string(
                'email_templates/confirm_registration.html')
            plain_text = strip_tags(html_message)
            send_mail(_('Thank You for Registration'), plain_text, settings.DEFAULT_FROM_EMAIL, [
                      instance.email], html_message=html_message)
    