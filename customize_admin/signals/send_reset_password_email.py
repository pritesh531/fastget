from django.contrib.auth.forms import PasswordResetForm
from django.conf import settings


def reset_password(request, email, from_email=settings.DEFAULT_FROM_EMAIL, template='registration/password_reset_email_html.html'):
    """
    Reset the password for all (active) users with given E-Mail adress
    """
    form = PasswordResetForm({'email': email})
    assert form.is_valid()
    return form.save(request=request, use_https=request.is_secure(), from_email=from_email, email_template_name=template)
