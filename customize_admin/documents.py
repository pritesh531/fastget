from django_elasticsearch_dsl import Document, fields
from django_elasticsearch_dsl.registries import registry
from .models import ServiceProvider


@registry.register_document
class ServiceProviderDocument(Document):
    user = fields.ObjectField(properties={
        'id': fields.IntegerField(),
        'username': fields.TextField(),
        'first_name': fields.TextField(),
        'last_name': fields.TextField(),
    })
    class Index:
        # Name of the Elasticsearch index
        name = 'service_providers'

        # See Elasticsearch Indices API reference for available settings
        settings = {'number_of_shards': 1,
                    'number_of_replicas': 0}

    class Django:
        model = ServiceProvider # The model associated with this Document

        # The fields of the model you want to be indexed in Elasticsearch
        fields = [
            'degree',
            'specialize',
            'location',
        ]

        def get_queryset(self):
            return super(PostDocument, self).get_queryset().select_related(
                'user'
            )

        # Ignore auto updating of Elasticsearch when a model is saved
        # or deleted:
        # ignore_signals = True

        # Don't perform an index refresh after every update (overrides global setting):
        # auto_refresh = False

        # Paginate the django queryset used to populate the index with the specified size
        # (by default it uses the database driver's default setting)
        # queryset_pagination = 5000